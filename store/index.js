import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  state: {
    vote: undefined,
    currentStage: undefined,
    stages: undefined,
    result: undefined
  },
  mutations: {
    createVote(state, newVote) {
      state.vote = newVote;
    },
    setCurrentStage(state, newStage) {
      state.currentStage = newStage;
    },
    setStages(state, stages) {
      state.stages = stages;
    },
    setResult(state, result) {
      state.result = result;
    }
  }
})

export default store
